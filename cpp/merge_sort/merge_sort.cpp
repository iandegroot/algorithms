#include <iostream>
#include <vector>

using std::vector;

class MergeSort
{
    // Enable/disable debug prints
    const static bool debug = false;

    // Used to swap two elements in a vector
    void swap(vector<int> &arr, int in1, int in2)
    {
        int temp = arr[in1];
        arr[in1] = arr[in2];
        arr[in2] = temp;
    }

    // Merges values in the given range together, adding them back to the list in a sorted order
    void merge(vector<int> &arr, int low, int mid, int high)
    {
        if (debug)
        {
            std::cout << "Merging " << low << " to " << high << " with mid " << mid << std::endl;
            print_vec(arr);
        }
        
        vector<int> lower;
        vector<int> upper;

        // Copy values from lower and upper ranges
        for (int i = low; i <= mid; i++)
            lower.push_back(arr[i]);

        for (int i = mid + 1; i <= high; i++)
            upper.push_back(arr[i]);

        int low_index = 0;
        int up_index = 0;
        int arr_index = low;

        // Merge values back into main array in the correct order

        // While indices for both vectors are still in range
        while (low_index < lower.size() && up_index < upper.size())
        {
            if (lower[low_index] <= upper[up_index])
            {
                arr[arr_index] = lower[low_index];
                low_index++;
            }
            else
            {
                arr[arr_index] = upper[up_index];
                up_index++;
            }

            arr_index++;
        }

        // Take care of left over values
        while (low_index < lower.size())
        {
            arr[arr_index] = lower[low_index];
            low_index++;
            arr_index++;
        }

        while (up_index < upper.size())
        {
            arr[arr_index] = upper[up_index];
            up_index++;
            arr_index++;
        }

        if (debug)
            print_vec(arr);
    }

    public:
        // Prints out all the elements in a vector
        void print_vec(vector<int> &arr)
        {
            for (int i = 0; i < arr.size(); i++)
                std::cout << arr[i] << " ";

            std::cout << std::endl;
        }

        // Merge sort base function
        void sort(vector<int> &arr, int low, int high)
        {
            if (low < high)
            {
                // Split the array
                int mid = (high + low) / 2;

                // Split lower
                sort(arr, low, mid);

                // Split upper
                sort(arr, mid + 1, high);

                // Sort while merging lower and upper ranges together
                merge(arr, low, mid, high);
            }
        }
};



int main()
{

    MergeSort merge;
    vector<int> test_list;


    test_list.push_back(9);
    test_list.push_back(0);
    test_list.push_back(8);
    test_list.push_back(1);
    test_list.push_back(8);
    test_list.push_back(3);
    test_list.push_back(5);

    merge.print_vec(test_list);
    merge.sort(test_list, 0, test_list.size() - 1);
    merge.print_vec(test_list);


    test_list.clear();
    test_list.push_back(8);
    test_list.push_back(7);
    test_list.push_back(8);

    merge.print_vec(test_list);
    merge.sort(test_list, 0, test_list.size() - 1);
    merge.print_vec(test_list);


    test_list.clear();
    test_list.push_back(1);
    test_list.push_back(2);
    test_list.push_back(3);
    test_list.push_back(4);
    test_list.push_back(5);
    test_list.push_back(6);
    test_list.push_back(7);
    test_list.push_back(8);

    merge.print_vec(test_list);
    merge.sort(test_list, 0, test_list.size() - 1);
    merge.print_vec(test_list);


    test_list.clear();
    test_list.push_back(8);
    test_list.push_back(7);
    test_list.push_back(6);
    test_list.push_back(5);
    test_list.push_back(4);
    test_list.push_back(3);
    test_list.push_back(2);
    test_list.push_back(1);

    merge.print_vec(test_list);
    merge.sort(test_list, 0, test_list.size() - 1);
    merge.print_vec(test_list);
}

