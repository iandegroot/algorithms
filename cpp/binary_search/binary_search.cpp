#include <iostream>
#include <vector>

using std::vector;

class BinarySearch
{
    // Enable/disable debug prints
    const static bool debug = false;

    public:
        // Prints out all the elements in a vector
        void print_vec(vector<int> &arr)
        {
            for (int i = 0; i < arr.size(); i++)
                std::cout << arr[i] << " ";

            std::cout << std::endl;
        }

        int find_val(vector<int> &arr, int val)
        {
            int mid = arr.size() / 2;
            int last_mid = mid;

            while (true) {
                if (val == arr[mid])
                {
                    return mid;
                }
                else if (val > arr[mid])
                {
                    if (val < arr[mid + 1])
                        return -1;
                    mid = (mid + arr.size()) / 2;
                }
                else if (val < arr[mid])
                {
                    if (val > arr[mid - 1])
                        return -1;
                    mid = mid / 2;
                }

                // Check that the same midpoint wasn't used as last time, if it was then the value does not exist in the list
                if (mid == last_mid)
                    return -1;
                else
                    last_mid = mid;
            }
        }
};



int main()
{

    BinarySearch search;
    vector<int> test_list;
    int search_val;
    int val_index;


    test_list.push_back(0);
    test_list.push_back(1);
    test_list.push_back(2);
    test_list.push_back(3);
    test_list.push_back(4);
    test_list.push_back(5);
    test_list.push_back(6);

    search_val = 5;
    search.print_vec(test_list);
    val_index = search.find_val(test_list, search_val);
    if (val_index != -1)
        std::cout << "The value " << search_val << " was found at index " << val_index << std::endl << std::endl;
    else
        std::cout << search_val << " was not found in the vector" << std::endl << std::endl;


    test_list.clear();
    test_list.push_back(2);
    test_list.push_back(100);
    test_list.push_back(566);

    search_val = 566;
    search.print_vec(test_list);
    val_index = search.find_val(test_list, search_val);
    if (val_index != -1)
        std::cout << "The value " << search_val << " was found at index " << val_index << std::endl << std::endl;
    else
        std::cout << search_val << " was not found in the vector" << std::endl << std::endl;


    test_list.clear();
    test_list.push_back(3);
    test_list.push_back(5);
    test_list.push_back(8);
    test_list.push_back(45);
    test_list.push_back(768);
    test_list.push_back(3455);
    test_list.push_back(4623);
    test_list.push_back(5675);

    search_val = 6;
    search.print_vec(test_list);
    val_index = search.find_val(test_list, search_val);
    if (val_index != -1)
        std::cout << "The value " << search_val << " was found at index " << val_index << std::endl << std::endl;
    else
        std::cout << search_val << " was not found in the vector" << std::endl << std::endl;


    test_list.clear();
    test_list.push_back(0);
    test_list.push_back(5);
    test_list.push_back(8);
    test_list.push_back(9);
    test_list.push_back(10);
    test_list.push_back(34);
    test_list.push_back(43);
    test_list.push_back(46);

    search_val = 100;
    search.print_vec(test_list);
    val_index = search.find_val(test_list, search_val);
    if (val_index != -1)
        std::cout << "The value " << search_val << " was found at index " << val_index << std::endl << std::endl;
    else
        std::cout << search_val << " was not found in the vector" << std::endl << std::endl;
}

