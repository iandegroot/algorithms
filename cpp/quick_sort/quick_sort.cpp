#include <iostream>
#include <vector>

using std::vector;

class QuickSort
{

    // Used to swap two elements in a vector
    void swap(vector<int> &arr, int in1, int in2)
    {
        int temp = arr[in1];
        arr[in1] = arr[in2];
        arr[in2] = temp;
    }

    // Takes the last element in the given range as the pivot and sorts all elements in range as either less or greater than pivot value
    int partition(vector<int> &arr, int low, int high)
    {
        /****** Primary algorithm ******/

        // Pick the last value to be the pivot value
        int pivot = arr[high];
        // Base will mark the first element greater than the pivot value
        int base = low;

        // Move through the array, looking for elements that are less than or equal to the pivot value
        // When found, swap those values with whatever index base is currently at
        for (int i = low; i < high; i++)
        {
            if (arr[i] <= pivot)
            {
                swap(arr, base, i);
                base++;
            }
        }

        // Swap the pivot with the first 'greater than' element
        swap(arr, base, high);
        // Return the index of the pivot
        return base;


        /****** Alternate algorithm ******/

        // // Pick the last value to be the pivot value
        // int pivot = arr[high];
        // int left = low;
        // int right = high - 1;
        // bool done = false;

        // while (!done)
        // {
        //     // Keep the left runner moving until it hits a value that's greater than the pivot element
        //     while (left <= right && arr[left] <= pivot)
        //         left++;

        //     // Keep the right runner moving until it hits a value that's greater than the pivot element
        //     while (arr[right] >= pivot && right >= left)
        //         right--;

        //     if (right < left)
        //         done = true;
        //     else
        //         swap(arr, left, right);
        // }

        // Swap the pivot with the first 'greater than' element
        //swap(arr, left, high);

        // Return the index of the pivot
        //return left;
    }

    public:
        // Prints out all the elements in a vector
        void print_vec(vector<int> &arr)
        {
            for (int i = 0; i < arr.size(); i++)
                std::cout << arr[i] << " ";

            std::cout << std::endl;
        }

        // Quick sort base function
        void sort(vector<int> &arr, int low, int high)
        {
            if (low < high)
            {
                // Partition
                int pivot_index = partition(arr, low, high);

                // Sort lower
                sort(arr, low, pivot_index - 1);

                // Sort upper
                sort(arr, pivot_index + 1, high);
            }
        }
};



int main()
{

    QuickSort quick;
    vector<int> test_list;


    test_list.push_back(9);
    test_list.push_back(0);
    test_list.push_back(8);
    test_list.push_back(1);
    test_list.push_back(8);
    test_list.push_back(3);
    test_list.push_back(5);

    quick.print_vec(test_list);
    quick.sort(test_list, 0, test_list.size() - 1);
    quick.print_vec(test_list);


    test_list.clear();
    test_list.push_back(8);
    test_list.push_back(7);
    test_list.push_back(8);

    quick.print_vec(test_list);
    quick.sort(test_list, 0, test_list.size() - 1);
    quick.print_vec(test_list);


    test_list.clear();
    test_list.push_back(1);
    test_list.push_back(2);
    test_list.push_back(3);
    test_list.push_back(4);
    test_list.push_back(5);
    test_list.push_back(6);
    test_list.push_back(7);
    test_list.push_back(8);

    quick.print_vec(test_list);
    quick.sort(test_list, 0, test_list.size() - 1);
    quick.print_vec(test_list);


    test_list.clear();
    test_list.push_back(8);
    test_list.push_back(7);
    test_list.push_back(6);
    test_list.push_back(5);
    test_list.push_back(4);
    test_list.push_back(3);
    test_list.push_back(2);
    test_list.push_back(1);

    quick.print_vec(test_list);
    quick.sort(test_list, 0, test_list.size() - 1);
    quick.print_vec(test_list);
}

