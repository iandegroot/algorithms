import unittest

class MergeSort:

    debug = True

    def merge_sort(self, list_to_sort):

        if self.debug:
            print("merge_sort:", list_to_sort)

        if (len(list_to_sort) > 1):
            mid = len(list_to_sort) // 2

            # Bottom half
            bottom_list = self.merge_sort(list_to_sort[0:mid])
            # Top half
            top_list = self.merge_sort(list_to_sort[mid:len(list_to_sort)])

            # Merge the sorted results
            list_to_sort = self.merge(bottom_list, top_list)

        return list_to_sort

    def merge(self, bot, top):
        new_list = []
        curr_bot = 0
        curr_top = 0

        # # While both runners are still in bounds
        # while curr_bot < len(bot) and curr_top < len(top):
        #     # Select which element to add to list
        #     if bot[curr_bot] < top[curr_top]:
        #         new_list.append(bot[curr_bot])
        #         curr_bot += 1
        #         # When the last bottom element has been reached then append all elements left from the top
        #         if curr_bot == len(bot):
        #             while curr_top < len(top):
        #                 new_list.append(top[curr_top])
        #                 curr_top += 1
        #     else:
        #         new_list.append(top[curr_top])
        #         curr_top += 1
        #         # When the last top element has been reached then append all elements left from the bottom
        #         if curr_top == len(top):
        #             while curr_bot < len(bot):
        #                 new_list.append(bot[curr_bot])
        #                 curr_bot += 1

        # While both runners are still in bounds
        while curr_bot < len(bot) and curr_top < len(top):
            # Select which element to add to list
            if bot[curr_bot] < top[curr_top]:
                new_list.append(bot[curr_bot])
                curr_bot += 1
            else:
                new_list.append(top[curr_top])
                curr_top += 1

        # Take care of left over elements
        # TODO add this to above loop
        # while curr_bot < len(bot) or curr_top < len(top):
        #     if curr_bot < len(bot):
        #         new_list.append(bot[curr_bot])
        #         curr_bot += 1
        #     elif curr_top < len(top):
        #         new_list.append(top[curr_top])
        #         curr_top += 1

        while curr_bot < len(bot):
            new_list.append(bot[curr_bot])
            curr_bot += 1

        while curr_top < len(top):
            new_list.append(top[curr_top])
            curr_top += 1

        if self.debug:
            print("merge:     ", new_list)
        return new_list




class MergeSortTest(unittest.TestCase):

    def test_sorted(self):
        test_lists = [[0, 4, 2, 56, 6, 2, 8, 65, 4], [8, 4, 76, 3, 7, 8, 3, 6, 1, 8], [6, 7, 3, 4, 8, 121, 64, 73, 98, 8, 1]]

        merger = MergeSort()

        # Check that all lists are sorted
        for test_list in test_lists:
            expected_test_list = sorted(test_list)
            print("Unsorted list: ", test_list)
            sorted_test_list = merger.merge_sort(test_list)
            print("Sorted list: ", sorted_test_list)
            self.assertEqual(expected_test_list, sorted_test_list)



if __name__ == "__main__":
    unittest.main()