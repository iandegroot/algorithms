import unittest

class BubbleSort:

    debug = True

    def bubble_sort(self, list_to_sort):

        for i in range(len(list_to_sort)):
            # Don't need to go all the way to the end in later iterations because the largest values will already be there
            for j in range(len(list_to_sort) - 1 - i):
                if list_to_sort[j] > list_to_sort[j + 1]:
                    list_to_sort[j], list_to_sort[j + 1] = list_to_sort[j + 1], list_to_sort[j]

        return list_to_sort



class BubbleSortTest(unittest.TestCase):

    def test_sorted(self):
        test_lists = [[0, 4, 2, 56, 6, 2, 8, 65, 4], [8, 4, 76, 3, 7, 8, 3, 6, 1, 8], [6, 7, 3, 4, 8, 121, 64, 73, 98, 8, 1]]

        bubble = BubbleSort()

        # Check that all lists are sorted
        for test_list in test_lists:
            expected_test_list = sorted(test_list)
            print("Unsorted list: ", test_list)
            sorted_test_list = bubble.bubble_sort(test_list)
            print("Sorted list: ", sorted_test_list)
            self.assertEqual(expected_test_list, sorted_test_list)



if __name__ == "__main__":
    unittest.main()