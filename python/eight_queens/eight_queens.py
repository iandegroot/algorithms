import copy

# Layout of the board

# row,column
###      0,0 0,1 0,2 0,3 0,4 0,5 0,6 0,7
###      1,0 1,1 1,2 1,3 1,4 1,5 1,6 1,7
###      2,0 2,1 2,2 2,3 2,4 2,5 2,6 2,7
###      3,0 3,1 3,2 3,3 3,4 3,5 3,6 3,7
###      4,0 4,1 4,2 4,3 4,4 4,5 4,6 4,7
###      5,0 5,1 5,2 5,3 5,4 5,5 5,6 5,7
###      6,0 6,1 6,2 6,3 6,4 6,5 6,6 6,7
###      7,0 7,1 7,2 7,3 7,4 7,5 7,6 7,7


class Coord:

    def __init__(self, r, c):
        self.row = r
        self.col = c


class EightQueens:

    def __init__(self):
        # 0 means there is no queen at that location on the board
        self.board = [[0 for _ in range(8)] for _ in range(8)]

    # Will scan the board from a starting location going left to right until the end of the row, then moving to the next row
    def find_solution(self, start, queen_num):
        if queen_num > 8:
            return True

        for row in range(len(self.board)):
            for col in range(len(self.board[row])):
                coord = Coord(row, col)
                if self.can_place(coord):
                    self.place_queen(coord)
                    print("Placed queen {} at {}, {}!".format(queen_num, coord.row, coord.col))
                    #self.print_board()
                    if self.find_solution(Coord(row + 1, 0), queen_num + 1):
                        return True
                    else:
                        self.remove_queen(coord)

        return False


    def print_board(self):
        for row in self.board:
            print(row)

    def place_queen(self, space):
        self.board[space.row][space.col] = 1

    def remove_queen(self, space):
        self.board[space.row][space.col] = 0

    def can_place(self, space):
        if self.check_vert_and_hori(space) and self.check_diag(space):
            return True
        else:
            #print("Can't place queen at {}, {}".format(space.row, space.col))
            return False

    def check_vert_and_hori(self, space):
        # Check if another queen is in the same row
        for i in range(8):
            if self.board[space.row][i] == 1:
                return False

        # Check if another queen is in the same column
        for i in range(8):
            if self.board[i][space.col] == 1:
                return False

        return True

    def check_diag(self, space):
        # Check if another queen is diagonal to this spot

        # Up and left
        checker = copy.deepcopy(space)
        while checker.row > -1 and checker.col > -1:
            if self.board[checker.row][checker.col] == 1:
                return False
            checker.row -= 1
            checker.col -= 1

        # Up and right
        checker = copy.deepcopy(space)
        while checker.row > -1 and checker.col < 8:
            if self.board[checker.row][checker.col] == 1:
                return False
            checker.row -= 1
            checker.col += 1

        # Down and left
        checker = copy.deepcopy(space)
        while checker.row < 8 and checker.col > -1:
            if self.board[checker.row][checker.col] == 1:
                return False
            checker.row += 1
            checker.col -= 1

        # Down and right
        checker = copy.deepcopy(space)
        while checker.row < 8 and checker.col < 8:
            #print(checker.row, checker.col)
            if self.board[checker.row][checker.col] == 1:
                return False
            checker.row += 1
            checker.col += 1

        return True


if __name__ == "__main__":

    queens = EightQueens()


    #queens.board[0][0] = 1
    # queens.board[1][2] = 1
    # print(queens.can_place(Coord(3, 0)))

    # queens.print_board()

    queens.find_solution(Coord(0, 0), 1)

    queens.print_board()
