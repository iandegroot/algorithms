import unittest

class QuickSort:

    debug = True

    def quick_sort(self, list_to_sort):

        if (len(list_to_sort) > 1):
            # Partition
            list_to_sort = self.partition(list_to_sort)

        return list_to_sort


    def partition(self, list_to_partition):
        if self.debug:
            print(list_to_partition)

        # Catch the base case of list with a single element
        if len(list_to_partition) < 2:
            return list_to_partition
        # Select the pivot element (always the last element due to convention)
        pivot = list_to_partition[-1]

        # Get indices of left and right runners
        left = 0
        right = len(list_to_partition) - 2

        while left <= right:
            # Move the left index to the right until it hits an element that's greater than pivot
            if list_to_partition[left] > pivot:
                # Now move the right index left until it hits an element that's less than the pivot
                while left < right:
                    # When found, swap the left and right elements and go back to moving the left index to the right
                    if list_to_partition[right] < pivot:
                        list_to_partition[right], list_to_partition[left] = list_to_partition[left], list_to_partition[right]
                        right -= 1
                        break

                    right -= 1

            left += 1

        # Swap pivot value into correct index
        if list_to_partition[left - 1] > pivot:
            left -= 1

        list_to_partition[-1], list_to_partition[left] = list_to_partition[left], list_to_partition[-1]

        # Call quick sort on low
        sorted_low = self.partition(list_to_partition[0:left])

        # Call quick sort on upper
        sorted_upper = self.partition(list_to_partition[left + 1:len(list_to_partition)])

        # Combine
        sorted_list = self.combine(sorted_low, list_to_partition[left], sorted_upper)

        return sorted_list


    def combine(self, low_list, pivot_val, upper_list):
        sorted_list = []
        runner = 0

        # Append lower list, pivot value, and the upper list
        # More pythonic way would be low_list.append(pivot) + upper_list
        for i in range(len(low_list)):
            sorted_list.append(low_list[i])

        sorted_list.append(pivot_val)

        for i in range(len(upper_list)):
            sorted_list.append(upper_list[i])

        return sorted_list




class QuickSortTest(unittest.TestCase):

    def test_sorted(self):
        test_lists = [[0, 4, 2, 56, 6, 2, 8, 65, 4], [8, 4, 76, 3, 7, 8, 3, 6, 1, 8], [6, 7, 3, 4, 8, 121, 64, 73, 98, 8, 1]]

        quick = QuickSort()

        # Check that all lists are sorted
        for test_list in test_lists:
            expected_test_list = sorted(test_list)
            print("Unsorted list: ", test_list)
            sorted_test_list = quick.quick_sort(test_list)
            print("Sorted list: ", sorted_test_list)
            self.assertEqual(expected_test_list, sorted_test_list)



if __name__ == "__main__":
    unittest.main()